jQuery(document).ready(function($){
    let clicked = false;

    $('body').on('click', '.checkbox-wrap', function (e) {
        if (clicked) return;

        clicked = true;

        if ( $(e.target).is($(this).find('> label')) ) {
            if ($('.checkbox-wrap > input').filter(':checked').length === 0) {
                $('.vaa-gallery-submit').addClass('vaa-gallery-submit__visible');
            } else if ($('.checkbox-wrap > input').filter(':checked').length === 1 && $(this).find('input').prop('checked')) {
                $('.vaa-gallery-submit').removeClass('vaa-gallery-submit__visible');
            }
        }

        setTimeout(function () {
            clicked = false;
        }, 100);
    });

    $(document).keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    $('body').on('click', '.vaa-gallery-submit', function (e) {
        e.preventDefault();

        if ($('.checkbox-wrap > input').filter(':checked').length === 0) {
            return;
        }

        $('#vaaGalleryForm').submit();
    });

    $('body').on('click', '#elementor-popup-modal-2370', function (e) {
        if ($(e.target).is('.attachment-large')) {
            setTimeout(function () {
                window.removeEventListener('click', p);
            }, 2000);
        }
    });
});