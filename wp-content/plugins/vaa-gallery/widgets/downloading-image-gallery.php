<?php
namespace DownloadingImageGallery;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor downloading image gallery widget.
 *
 * Elementor widget that displays a set of images you can download in an aligned grid.
 *
 * @since 1.0.0
 */
class Widget_Downloading_Image_Gallery extends \Elementor\Widget_Image_Gallery {

	/**
	 * Get widget name.
	 *
	 * Retrieve downloading image gallery widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'downloading-image-gallery';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve downloading image gallery widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Downloading Image Gallery', 'elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve downloading image gallery widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
        return 'fa fa-code';
	}

    /**
     * Get widget categories.
     *
     * Retrieve downloading image gallery widget categories.
     *
     * @since 1.0.0
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'general' ];
    }

    public function add_checkbox_data_to_image_link( $link_html, $id ) {
        $lightbox_link_html = parent::add_lightbox_data_to_image_link($link_html);
        $checkbox = '<input type="checkbox" name="images[]" value="' . $id . '" id="' . $id . '"><label for="' . $id . '">&nbsp;</label>';

        $wrap = '<div class="checkbox-wrap">'.$checkbox;
        $closeWrap = '</div>';

        $output = preg_replace( '/^<a/', $wrap . '<a', $lightbox_link_html );
        $output = preg_replace('/<\/a>$/', '</a>' . $closeWrap, $output);

        return $output;
    }

    protected function _register_controls() {
        parent::_register_controls();

        $this->start_controls_section('section_downloading', [
            'label' => __('Downloading', $this->get_name()),
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
        ]);

        $this->add_control('selecting_checkbox', [
            'label' => __( 'Show Checkboxes', $this->get_name() ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Yes', $this->get_name() ),
            'label_off' => __( 'No', $this->get_name() ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]);

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display();

        if ( ! $settings['wp_gallery'] ) {
            return;
        }

        $ids = wp_list_pluck( $settings['wp_gallery'], 'id' );

        $this->add_render_attribute( 'shortcode', 'ids', implode( ',', $ids ) );
        $this->add_render_attribute( 'shortcode', 'size', $settings['thumbnail_size'] );

        if ( $settings['gallery_columns'] ) {
            $this->add_render_attribute( 'shortcode', 'columns', $settings['gallery_columns'] );
        }

        if ( $settings['gallery_link'] ) {
            $this->add_render_attribute( 'shortcode', 'link', $settings['gallery_link'] );
        }

        if ( ! empty( $settings['gallery_rand'] ) ) {
            $this->add_render_attribute( 'shortcode', 'orderby', $settings['gallery_rand'] );
        }


        ?>

        <div class="vaa-gallery">
            <form id="vaaGalleryForm" action="/wp-content/plugins/vaa-gallery/widgets/download/" method="post">
                <?php
                $this->add_render_attribute( 'link', [
                    'data-elementor-open-lightbox' => $settings['open_lightbox'],
                    'data-elementor-lightbox-slideshow' => $this->get_id(),
                    'class' => 'downloading-image-gallery-link',
                ] );

                add_filter( 'wp_get_attachment_link', [ $this, 'add_checkbox_data_to_image_link' ], 10, 2 );

                echo do_shortcode( '[gallery ' . $this->get_render_attribute_string( 'shortcode' ) . ']' );

                remove_filter( 'wp_get_attachment_link', [ $this, 'add_checkbox_data_to_image_link' ], 10 );
                ?>
                <button type="submit" class="vaa-gallery-submit"></button>
            </form>
        </div>
        <?php
    }
}
