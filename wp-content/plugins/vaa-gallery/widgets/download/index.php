<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

if (!$_POST['images']) {
   exit();
}

// Путь сохранения архива
$uploads_dir = wp_get_upload_dir();
$archive_dir = $uploads_dir['basedir'] . '/vaa-gallery';
// Создание zip архива
$zip = new ZipArchive();
// Имя файла архива
$archive_short_name = 'photos_'.date('jmYhis').'.zip';
$archive_name = $archive_dir.'/'.$archive_short_name;

if ($zip->open($archive_name, ZIPARCHIVE::CREATE) !== true) {
    fwrite(STDERR, "Error while creating archive file");
    exit(1);
}

$images = $_POST['images'];

foreach ($images as $image) {
    $path = get_attached_file($image);
    $pathChunks = explode('/', $path);
    // Папка картинки
    $dir = '';
    // Имя файла
    $img = $pathChunks[count($pathChunks) - 1];

    for ($i = 0; $i < count($pathChunks) - 1; $i++) {
        $dir = $dir . $pathChunks[$i] . '/';
    }

    $zip->addFile($path, $img);
}

// Закрываем архив
$zip->close();
header('Content-Type: application/zip');
header('Content-Disposition: attachment; filename="'.$archive_short_name.'"');
readfile($archive_name);

if (file_exists($archive_dir)) {
    foreach (glob($archive_dir . '/*') as $file) {
        unlink($file);
    }
};

?>

