<?php
/**
 * Plugin Name: Vaa-gallery
 * Description: Extension for Elementor Gallery Widget
 * Author: Vashchenko Aleksey
 * Version: 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * Main Vaa Gallery Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class Vaa_Gallery {

    /**
     * Plugin Version
     *
     * @since 1.0.0
     *
     * @var string The plugin version.
     */
    const VERSION = '1.0.0';

    /**
     * Minimum Elementor Version
     *
     * @since 1.0.0
     *
     * @var string Minimum Elementor version required to run the plugin.
     */
    const MINIMUM_ELEMENTOR_VERSION = '2.6.6';

    /**
     * Minimum PHP Version
     *
     * @since 1.0.0
     *
     * @var string Minimum PHP version required to run the plugin.
     */
    const MINIMUM_PHP_VERSION = '7.1.26';

    /**
     * Instance
     *
     * @since 1.0.0
     *
     * @access private
     * @static
     *
     * @var Vaa_Gallery The single instance of the class.
     */
    private static $_instance = null;

    /**
     * Instance
     *
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @since 1.0.0
     *
     * @access public
     * @static
     *
     * @return Vaa_Gallery An instance of the class.
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct() {
        add_action( 'plugins_loaded', [ $this, 'init' ] );
    }

    /**
     * Initialize the plugin
     *
     * Load the plugin only after Elementor (and other plugins) are loaded.
     * Checks for basic plugin requirements, if one check fail don't continue,
     * if all check have passed load the files required to run the plugin.
     *
     * Fired by `plugins_loaded` action hook.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init() {
        // Check if Elementor installed and activated
        if ( ! did_action( 'elementor/loaded' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] ); // Need to realize
            return;
        }

        // Check for required Elementor version
        if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] ); // Need to realize
            return;
        }

        // Check for required PHP version
        if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] ); // Need to realize
            return;
        }

        // Add Plugin actions
        add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

        // Register Widget Styles
        add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'widget_styles' ] );

        // Register Widget Scripts
        add_action( 'elementor/frontend/after_register_scripts', [ $this, 'widget_scripts' ] );

        // Add new thumbnail size
        add_action( 'init', [ $this, 'add_new_thumbnail_size' ] );
    }

    /**
     * Admin notice
     *
     * Warning when the site doesn't have Elementor installed or activated.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function admin_notice_missing_main_plugin() {

        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

        $message = sprintf(
        /* translators: 1: Plugin name 2: Elementor */
            esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'elementor-test-extension' ),
            '<strong>' . esc_html__( 'Vaa Gallery', 'vaa-gallery' ) . '</strong>',
            '<strong>' . esc_html__( 'Elementor', 'vaa-gallery' ) . '</strong>'
        );

        printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

    }

    /**
     * Admin notice
     *
     * Warning when the site doesn't have a minimum required Elementor version.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function admin_notice_minimum_elementor_version() {

        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

        $message = sprintf(
        /* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
            esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'vaa-gallery' ),
            '<strong>' . esc_html__( 'Vaa Gallery', 'vaa-gallery' ) . '</strong>',
            '<strong>' . esc_html__( 'Elementor', 'vaa-gallery' ) . '</strong>',
            self::MINIMUM_ELEMENTOR_VERSION
        );

        printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

    }

    /**
     * Admin notice
     *
     * Warning when the site doesn't have a minimum required PHP version.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function admin_notice_minimum_php_version() {

        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

        $message = sprintf(
        /* translators: 1: Plugin name 2: PHP 3: Required PHP version */
            esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'vaa-gallery' ),
            '<strong>' . esc_html__( 'Vaa Gallery', 'vaa-gallery' ) . '</strong>',
            '<strong>' . esc_html__( 'PHP', 'vaa-gallery' ) . '</strong>',
            self::MINIMUM_PHP_VERSION
        );

        printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

    }

    /**
     * Init Widgets
     *
     * Include widgets files and register them
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init_widgets() {

        // Include Widget files
        require_once( __DIR__ . '/widgets/downloading-image-gallery.php' );

        // Register widget
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \DownloadingImageGallery\Widget_Downloading_Image_Gallery() );

    }

    /**
     * Widget Styles
     *
     * Include widget styles (css files)
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function widget_styles() {
        wp_register_style( 'vaa-gallery-css', plugins_url( 'css/styles.css', __FILE__ ) );
        wp_enqueue_style('vaa-gallery-css');
    }

    /**
     * Widget Scripts
     *
     * Include widget js scripts
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function widget_scripts() {
        wp_register_script( 'vaa-gallery-js', plugins_url( 'js/script.js', __FILE__ ), [ 'jquery' ] );
        wp_enqueue_script('vaa-gallery-js');
    }

    /**
     * Add New Thumbnail Size
     *
     * Crops only images that will be uploaded after
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function add_new_thumbnail_size() {
        add_image_size( 'vaa-gallery', 300, 300, true );

        add_filter( 'image_size_names_choose', 'new_custom_sizes' );

        function new_custom_sizes( $sizes ) {
            return array_merge( $sizes, array(
                'vaa-gallery' => 'Cropped 300x300'
            ) );
        }
    }
}
Vaa_Gallery::instance();